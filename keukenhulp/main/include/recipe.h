#ifndef RECIPE
#define RECIPE

typedef struct recipe_text
{
    char line[20];
} recipe_text_t;

typedef struct recipe
{
    recipe_text_t *recipe;
    int count;
} recipe_t;

void displayRawRecipeString(char *recipeRawString);

#endif