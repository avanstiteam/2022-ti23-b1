#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "recipe.h"
#include "rotaryencoder.h"
#include "LCD_wrapper.h"
#include "component_menu.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

const char *TAG = "RECIPE";

int current_line;
recipe_t *recipeT;

static void displayText();
static void setRotoryEncoder();
static void displayRecipe();
static void formatRawStringToLines();

void displayRawRecipeString(char *rawRecipeText)
{
	recipe_t *recipePTR = malloc(sizeof(recipe_t));
	recipePTR->recipe = NULL;
	// recipe_text_t *recipeTextLines = malloc(sizeof(recipe_text_t));
	// recipePTR->recipe = recipeTextLines;

	// ESP_LOGI(TAG, "Recipe text: %s", recipeRawString);
	if (recipePTR == NULL)
	{
		// ESP_LOGE(TAG, "memory error stringResipeFormat");
		return;
	}

	formatRawStringToLines(rawRecipeText, recipePTR);

	recipeT = recipePTR;
	displayRecipe(recipePTR);
}

static void displayRecipe(void *recipe)
{
	current_line = 0;
	ESP_LOGI(TAG, "made it to display recipe");
	recipeT = (recipe_t *)recipe;

	setRotoryEncoder();
	displayText();
}

// function to allocate memory for recipe text;
static recipe_text_t *addLine(recipe_text_t *recipe, int lineCount, bool *err)
{
	// vTaskDelay(1000 / portTICK_PERIOD_MS);
	ESP_LOGI(TAG, "linecount is %d", lineCount);
	recipe_text_t *tmp = realloc(recipe, lineCount * sizeof(recipe_text_t));
	if (tmp != NULL)
	{
		printf("allocated pointer %p", tmp);
		for (int i = 0; i < 20; i++)
		{
			tmp[lineCount - 1].line[i] = ' ';
		}
		return tmp;
	}
	else
	{
		*err = true;
		ESP_LOGE(TAG, "realloc failed: memory error in function stringResipeFormat");
		return recipe;
	}
}

static void replaceLastWordWithSpace(int count, int lineCount, recipe_text_t *recipeTextLines)
{

	// printf("word got cut off early letter cKount %d, index %d, last space index%d\n", count, index, lastSpaceIndex);
	for (int i = 0; i < count; i++)
	{
		// printf("should be char: '%c' is char '%c'\n", rawRecipeText[lastSpaceIndex + i], tmpChar);

		recipeTextLines[lineCount].line[i] = recipeTextLines[lineCount - 1].line[(20 - count) + i];
		;
		recipeTextLines[lineCount - 1].line[(20 - count) + i] = ' ';
	}
}

static void formatRawStringToLines(char *rawRecipeText, recipe_t *recipePTR)
{
	recipe_text_t *recipeTextLines = recipePTR->recipe;
	int lineCount = -1;
	int lastSpaceIndex = -1;
	int lineCharIndex = 20;
	int index = 0;

	bool err = false;
	while (rawRecipeText[index])
	{
		char curLetter = rawRecipeText[index];
		// printf("%c", curLetter);
		// ESP_LOGI(TAG, "current letter is: %c", curLetter);
		//  check if there is space on this line
		if (lineCharIndex > 19)
		{
			// ESP_LOGI(TAG, "%s", recipe[lineCount].line);
			lineCount++;
			lineCharIndex = 0;
			recipeTextLines = addLine(recipeTextLines, lineCount + 1, &err);

			if (err)
				return;

			// calculate letters sinds last space index
			int count = index - (lastSpaceIndex + 1);

			// check if last space index is not in the future or more than the max charecters
			if (count > 0 && count < 19)
			{
				replaceLastWordWithSpace(count, lineCount, recipeTextLines);
				lineCharIndex = count;
			}
		}

		switch (curLetter)
		{
		case '\n':
			// check if there is another line
			if (rawRecipeText[index + 1])
			{
				lineCount++;
				lineCharIndex = 0;
				// allocate the new line
				recipeTextLines = addLine(recipeTextLines, lineCount + 1, &err);
				if (err)
					return;
				// printf("\n");
			}
			break;
		case ' ':
			lastSpaceIndex = index;
		default:
			recipeTextLines[lineCount].line[lineCharIndex] = curLetter;
			lineCharIndex++;
			break;
		}

		index++;
	}

	recipePTR->recipe = recipeTextLines;
	recipePTR->count = lineCount + 1;
}

static void displayText()
{
	LCD_clear();
	for (int i = 0; i < 4; i++)
	{
		int index = current_line + i;
		if (index > recipeT->count - 1)
		{
			LCD_write("::End of recipe::", i);
			ESP_LOGI(TAG, "current line is current %d index is %d", i, index);
			break;
		}
		char *text = recipeT->recipe[index].line;
		LCD_write(text, i);
	}
}

static void clear()
{
	printf("allocated pointer %p", recipeT->recipe);
	free(recipeT->recipe);
	free(recipeT);
	current_line = 0;
}

static void recipe_onLeftTwist()
{
	ESP_LOGI(TAG, "Left TWIST");
	if (current_line < 1)
	{
		return;
	}
	current_line--;
	displayText();
}

static void recipe_onRightTwist()
{
	ESP_LOGI(TAG, "Right TWIST");
	if (current_line > recipeT->count - 4)
	{
		return;
	}
	current_line++;
	displayText();
}

static void recipe_onClick()
{
	clear();
	setup_menu_controls();
}

static void setRotoryEncoder()
{
	rotary_setonaction(recipe_onLeftTwist, ONTWISTLEFT);
	rotary_setonaction(recipe_onRightTwist, ONTWISTRIGHT);
	rotary_setonaction(recipe_onClick, ONCLICK);
}
