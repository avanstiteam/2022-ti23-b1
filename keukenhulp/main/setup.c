#include <stdio.h>
#include <stdbool.h>
#include "setup.h"
#include "rotaryencoder.h"
#include "audio_pipeline.h"
#include "audio_element.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "mcpWrapper.h"
#include "driver/i2c.h"
#include "seven_segment.h"
#include "WIFI.h"
#include "Bluetooth.h"
#include "bt_sink.h"
#include "sntp_sync.h"
#include "board.h"
#include "time.h"
#include "LCD_wrapper.h"
#include "component_menu.h"
#include "component_radio.h"
#include "klok.h"
#include "audioPipeline.h"
#include "talking_clock.h"
#include "audio_commands.h"
#include "component_timer.h"
#include "recipe.h"
#include "WIFI.h"
#include <stdlib.h>

#define I2C_MASTER_NUM 0
#define I2C_MASTER_TX_BUF_LEN 0 // disabled
#define I2C_MASTER_RX_BUF_LEN 0 // disabled
#define I2C_MASTER_FREQ_HZ 100000
#define I2C_MASTER_SDA_IO 18
#define I2C_MASTER_SCL_IO 23

#define SUBMENU_AMOUNT 10

bool RadioRunning = false;
bool BluetoothRunning = false;
char *strftime_buf[20];
TaskHandle_t radioTaskHandle;
TaskHandle_t sinkTaskHandle;
TaskHandle_t commandTaskHandle;

static void setup_menu(MENU_WINDOW *);
static void radio_toggle();
static void audio_codec_init();

// Sets up I2C for the rotary encoder
static void i2c_init(void)
{

    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE; // GY-2561 provides 10kΩ pullups
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE; // GY-2561 provides 10kΩ pullups
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;

    i2c_set_timeout(I2C_MASTER_NUM, 20000);
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_LEN, I2C_MASTER_TX_BUF_LEN, 0);
}

// setup for device, sets up all necessary components for fully working application
void audiocommandCallback()
{
    printf("Testing audio command 1\n");
}

void audiocommandCallback1()
{
    printf("Testing audio command 2\n");
}

AUDIO_COMMAND audiocommand = {
    .callback = audiocommandCallback,
    .frequency = 800,
};

AUDIO_COMMAND audiocommand1 = {
    .callback = audiocommandCallback1,
    .frequency = 1000,
};

#define COMMAND_COUNT 2
AUDIO_COMMAND array[COMMAND_COUNT];

// setup for device, sets up all necessary components for fully working application
void setup(void *ptr)
{

    MENU_WINDOW *submenuList = malloc(SUBMENU_AMOUNT * sizeof(MENU_WINDOW));
    setup_menu(submenuList);

    // printf("Setup for components \n");
    // ATTENTION: commands_init needs to be called before i2c_init because i2s needs to be called before i2c

    wifi_init();

    audio_codec_init();

    bluetooth_init();

    pipeline_init();

    array[0] = audiocommand;
    array[1] = audiocommand1;
    commands_init(array, COMMAND_COUNT);

    i2c_init();
    rotary_init();
    //MCP_init();
    LCD_init();
    LCD_clear();
    // seg7_init(1, 2, 3);
    menu_init(&submenuList[0]);
    //syncTime_init();
    // klok_init();
    //talking_clock_init();
    
    //pipeline_set(bt_config, 2);

    vTaskDelay(4000 / portTICK_PERIOD_MS);

    vTaskDelete(NULL);
}

static void radio_toggle()
{
    if (RadioRunning)
    {
        radio_stop();
        RadioRunning = false;
    }
    else
    {
        xTaskCreatePinnedToCore(radio_init, "radio init", 10000, NULL, 1, radioTaskHandle, 1);
        RadioRunning = true;
    }
}

static void sink_toggle(){
    if (BluetoothRunning)
    {
        sink_stop();
        BluetoothRunning = false;
    }
    else
    {
        xTaskCreatePinnedToCore(sink_init, "sink init", 10000, NULL, 1, sinkTaskHandle, 1);
        BluetoothRunning = true;
    }
}

static void play_time_task(void *ptr)
{
    talking_clock_say_current_time();
    vTaskDelete(NULL);
}

static void play_time()
{
    xTaskCreatePinnedToCore(play_time_task, "play time", 8196, NULL, 1, NULL, 1);
}

static void audio_toggle()
{
    audioCommands_setState(!audioCommands_getState());
}

static void audio_codec_init()
{
    audio_board_handle_t board_handle = audio_board_init();
    audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START);
    audio_hal_set_volume(board_handle->audio_hal, AUDIO_HAL_VOL_DEFAULT);
}

static char *RecipeRawString = "dit is een voorbeeld recept.\n de ingredienten zijnn: \n 2 eieren M \n peper en zout\n\n bak de eiren binnen 5 min gaar, voeg zout en peper toe naar wens.";

static void setup_menu(MENU_WINDOW *submenuList)
{

    MENU_WINDOW splash = {
        .title = "Keukenhulp",
        {{
             .text = "",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[1],
         },
         {
             .text = "Klik om te starten",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[1],
         },
         {
             .text = "",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[1],
         }},
        .hidePointer = true,
        .totalsubmenus = 3,
    };

    MENU_WINDOW options = {
        .title = "Opties",
        {{
             .text = "Radio",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[2],
         },
         {
             .text = "Audio modus",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[7],
         },
         {
             .text = "Klok",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[4],
         },
         {
             .text = "Timer",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[3],
         },
         {
             .text = "Bluetooth",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[8],
         },
         {
             .text = "Recipe",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[6],
         },
         {
             .text = "Terug",
             .functionPointer = menu_move_window,
             .param = (void *)&submenuList[0],
         }},
        .totalsubmenus = 7,
    };
    MENU_WINDOW audio_modus = {
        .title = "Audio modus",
        .lines = {
            {
                .text = "Aan/Uit",
                .functionPointer = audio_toggle,
                .param = NULL,
            },
            {
                .text = "Terug",
                .functionPointer = menu_move_window,
                .param = &submenuList[1],
            },
        },
        .totalsubmenus = 2,
    };

    MENU_WINDOW radio_menu = {
        .title = "Radio",
        .lines = {
            {
                .text = "Aan/Uit",
                .functionPointer = radio_toggle,
                .param = NULL,
            },
            {
                .text = "Kanalen",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[5],
            },
            {
                .text = "Terug",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[1],
            }},
        .totalsubmenus = 3,
    };

    MENU_WINDOW bt_menu = {
        .title = "Bluetooth",
        .lines = {
            {
                .text = "Aan/Uit",
                .functionPointer = sink_toggle,
                .param = NULL,
            },
            {
                .text = "BT-Opties",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[9],
            },
            {
                .text = "Terug",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[1],
            }},
        .totalsubmenus = 3,
    };

    MENU_WINDOW bt_opties_menu = {
        .title = "BT-Opties",
        .lines = {
            {
                .text = "Pause",
                .functionPointer = sink_pause,
                .param = NULL,
            },
            {
                .text = "Volgende",
                .functionPointer = sink_next,
                .param = (void *)&submenuList[1],
            },
            {
                .text = "Vorige",
                .functionPointer = sink_previous,
                .param = (void *)&submenuList[1],
            },
            {
                .text = "Terug",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[8],
            }},
        .totalsubmenus = 4,
    };

    int a = 0, b = 1, c = 2;
    MENU_WINDOW radio_channels = {
        .title = "Radio kanalen",
        .lines = {
            {
                .text = "Radio 538",
                .functionPointer = radio_set_channel,
                .param = (void *)a,
            },
            {
                .text = "Radio Classic",
                .functionPointer = radio_set_channel,
                .param = (void *)b,
            },
            {
                .text = "Radio 10",
                .functionPointer = radio_set_channel,
                .param = (void *)c,
            },
            {
                .text = "Terug",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[2],
            },
        },
        .totalsubmenus = 4,
    };

    MENU_WINDOW timer_menu = {
        .title = "Timer",
        .lines = {
            {
                .text = "xx:xx:xx",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[1],
                .centerText = true,
            },
            {
                .text = "",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[1],
            },
            {
                .text = "Klik voor terug",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[1],
            }},
        .hidePointer = true,
        .totalsubmenus = 3,
    };

    MENU_WINDOW klok_menu = {
        .title = "Klok",
        .lines = {
            {
                .text = "Lees voor",
                .functionPointer = play_time,
                .param = NULL,
            },
            {
                .text = "Terug",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[1],
            },
        },
        .totalsubmenus = 2,
    };
    MENU_WINDOW recipe_menu = {
        .title = "recipe",
        .lines = {
            {
                .text = "show",
                .functionPointer = displayRawRecipeString,
                .param = (void *)RecipeRawString,
            },
            {
                .text = "Klik voor terug",
                .functionPointer = menu_move_window,
                .param = (void *)&submenuList[1],
            },
        },
        .hidePointer = false,
        .totalsubmenus = 2,
    };

    submenuList[0] = splash;
    submenuList[1] = options;
    submenuList[2] = radio_menu;
    submenuList[3] = timer_menu;
    submenuList[4] = klok_menu;
    submenuList[5] = radio_channels;
    submenuList[6] = recipe_menu;
    submenuList[7] = audio_modus;
    submenuList[8] = bt_menu;
    submenuList[9] = bt_opties_menu;
}