

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "Bluetooth.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_peripherals.h"
#include "bluetooth_service.h"

static const char *TAG = "BLUETOOTH_STATION";

void bluetooth_init()
{
    ESP_LOGI(TAG, "Starting Bluetooth with device name %s", CONFIG_BT_DEVICE_NAME);
    bluetooth_service_cfg_t bt_cfg = {
        .device_name = CONFIG_BT_DEVICE_NAME,
        .mode = BLUETOOTH_A2DP_SINK,
    };
    bluetooth_service_start(&bt_cfg);
}

