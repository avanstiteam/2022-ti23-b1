#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "mcpWrapper.h"
#include "seven_segment.h"
#include "rotaryencoder.h"
#include "LCD_wrapper.h"
#include "component_menu.h"
#include "WIFI.h"
#include "nvs_flash.h"
#include "sntp_sync.h"
#include "klok.h"
#include "setup.h"

void app_main(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    xTaskCreatePinnedToCore(setup, "Setup task", 10000, NULL, 1, NULL, 1);
}