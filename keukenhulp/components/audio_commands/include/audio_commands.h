
#ifndef audio_commands_h //include guard
#define audio_commands_h
#include <stdbool.h>

/**
 * @brief A struct to add an audio command to a certain frequency
 * 
 */
typedef struct AUDIO_COMMAND{
    void* callback;
    int frequency;
} AUDIO_COMMAND;

void commands_init(AUDIO_COMMAND *array, int arraysize);
esp_err_t audioCommands_setState(bool state);
bool audioCommands_getState();

#endif //end include guard audio_commands