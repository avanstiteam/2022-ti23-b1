/**
 * Example ESP-ADF application using a Goertzel filter
 *
 * This application is an adaptation of the
 * Voice activity detection (VAD) example application in the
 * ESP-ADF framework by Espressif
 * https://github.com/espressif/esp-adf/tree/master/examples/speech_recognition/vad
 *
 * Goertzel algoritm initially implemented by P.S.M. Goossens,
 * adapted by Hans van der Linden
 *
 * Avans Hogeschool, Opleiding Technische Informatica
 */

#include <math.h>

#include "freertos/FreeRTOS.h"
#include "audioPipeline.h"
#include "esp_err.h"
#include "esp_log.h"
#include "board.h"
#include "audio_common.h"
#include "audio_pipeline.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "filter_resample.h"
#include "audio_commands.h"
#include "goertzel_filter.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

static const char *TAG = "GOERTZEL-EXAMPLE";
#define GOERTZEL_SAMPLE_RATE_HZ 8000 // Sample rate in [Hz]
#define GOERTZEL_FRAME_LENGTH_MS 100 // Block length in [ms]

#define GOERTZEL_BUFFER_LENGTH (GOERTZEL_FRAME_LENGTH_MS * GOERTZEL_SAMPLE_RATE_HZ / 1000) // Buffer length in samples

#define GOERTZEL_DETECTION_THRESHOLD 50.0f // Detect a tone when log magnitude is above this value

#define AUDIO_SAMPLE_RATE 48000 // Audio capture sample rate [Hz]
#define WAIT_POLL 5000

int32_t currentTime;
int32_t lastTrigger;

AUDIO_COMMAND* GOERTZEL_DETECT_FREQS;
int GOERTZEL_NR_FREQS;

void tone_detection_task(void *ptr);

/**
 * Determine if a frequency was detected or not, based on the magnitude that the
 * Goertzel filter calculated
 * Use a logarithm for the magnitude
 */
static void detect_freq(int target_freq, float magnitude, void *func)
{
    
    float logMagnitude = 10.0f * log10f(magnitude);
    if (logMagnitude > GOERTZEL_DETECTION_THRESHOLD)
    {
        ((void (*)())func)();
        lastTrigger = currentTime;
    }
}


static bool curentState = false;
esp_err_t audioCommands_setState(bool state){
    if(curentState == state)
    {
        return ESP_FAIL;
    }
    curentState = state;
    if(curentState){
    xTaskCreatePinnedToCore(tone_detection_task, "Setup task", 10000, NULL, 1, NULL, 1);
    }
    else {
        pipeline_reset();
    }
    return ESP_OK;
}

bool audioCommands_getState(){
    return curentState;
}


void tone_detection_task(void *ptr)
{
    pipeline_reset();
    audio_pipeline_handle_t *pipeline;

    audio_element_handle_t i2s_stream_reader;
    audio_element_handle_t resample_filter;
    audio_element_handle_t raw_reader;

    goertzel_filter_cfg_t filters_cfg[GOERTZEL_NR_FREQS];
    goertzel_filter_data_t filters_data[GOERTZEL_NR_FREQS];

    ESP_LOGI(TAG, "Number of Goertzel detection filters is %d", GOERTZEL_NR_FREQS);

    ESP_LOGI(TAG, "Create raw sample buffer");
    int16_t *raw_buffer = (int16_t *)malloc((GOERTZEL_BUFFER_LENGTH * sizeof(int16_t)));
    if (raw_buffer == NULL)
    {
        ESP_LOGE(TAG, "Memory allocation for raw sample buffer failed");
        vTaskDelete(NULL);
    }

    ESP_LOGI(TAG, "Setup Goertzel detection filters");
    for (int f = 0; f < GOERTZEL_NR_FREQS; f++)
    {
        filters_cfg[f].sample_rate = GOERTZEL_SAMPLE_RATE_HZ;
        filters_cfg[f].target_freq = (*(GOERTZEL_DETECT_FREQS + f)).frequency;
        filters_cfg[f].buffer_length = GOERTZEL_BUFFER_LENGTH;
        esp_err_t error = goertzel_filter_setup(&filters_data[f], &filters_cfg[f]);
        ESP_ERROR_CHECK(error);
    }

    ESP_LOGI(TAG, "Create pipeline");
    pipeline = pipeline_set(commands_config, 3);

    ESP_LOGI(TAG, "Get i2s audio stream");
    i2s_stream_reader = audio_pipeline_get_el_by_tag(*pipeline, "i2s_in");

    ESP_LOGI(TAG, "Get audio filter");
    resample_filter = audio_pipeline_get_el_by_tag(*pipeline, "resample");

    ESP_LOGI(TAG, "Get raw audio stream");
    raw_reader = audio_pipeline_get_el_by_tag(*pipeline, "raw");

    ESP_LOGI(TAG, "Start pipeline");
    pipeline_start();

    while (curentState)
    {
        raw_stream_read(raw_reader, (char *)raw_buffer, GOERTZEL_BUFFER_LENGTH * sizeof(int16_t));
        for (int f = 0; f < GOERTZEL_NR_FREQS; f++)
        {
            currentTime = xTaskGetTickCount();
            float magnitude;

            esp_err_t error = goertzel_filter_process(&filters_data[f], raw_buffer, GOERTZEL_BUFFER_LENGTH);
            ESP_ERROR_CHECK(error);

            if (goertzel_filter_new_magnitude(&filters_data[f], &magnitude) && (currentTime - lastTrigger) * 10 > WAIT_POLL)
            {
                detect_freq(filters_cfg[f].target_freq, magnitude, GOERTZEL_DETECT_FREQS[f].callback);
            }
        }
    }

    // Clean up (if we somehow leave the while loop, that is...)
    ESP_LOGI(TAG, "Deallocate raw sample buffer memory");
    free(raw_buffer);
    vTaskDelete(NULL);
}

/**
 * @brief Init audio commands handler
 *
 * @param array Audio command array
 * @param arraysize Audio command array size
 */
void commands_init(AUDIO_COMMAND* array, int arraysize)
{
    ESP_LOGI(TAG, "Init audio commands");
    GOERTZEL_DETECT_FREQS = array;
    GOERTZEL_NR_FREQS = arraysize;
    
    for(int i = 0; i < GOERTZEL_NR_FREQS; i++) { 
        ESP_LOGI(TAG, "Frequency: %d", (*(GOERTZEL_DETECT_FREQS + i)).frequency);
    }
}