#ifndef COMPONENT_RADIO_H
#define COMPONENT_RADIO_H

#include <stdio.h>
#include <stdbool.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include <stdbool.h>

void radio_init();
void radio_reset();
void radio_delete();
void radio_decrease_channel();
void radio_increase_channel();
void radio_set_channel(void *);
void radio_increase_volume();
void radio_decrease_volume();
void radio_stop();

#endif