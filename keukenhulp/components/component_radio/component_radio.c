#include "component_radio.h"

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "sdkconfig.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "http_stream.h"
#include "i2s_stream.h"
#include "aac_decoder.h"
#include "audioPipeline.h"
#include "esp_peripherals.h"
#include "periph_touch.h"
#include "periph_adc_button.h"
#include "periph_button.h"
#include "periph_wifi.h"
#include "board.h"
#include "filter_resample.h"
#include "audio_mem.h"

#include "input_key_service.h"
#include "audio_alc.h"

static const char *TAG = "INTERNET_RADIO";

// global radio volume
int volume = AUDIO_HAL_VOL_DEFAULT;

int currentUrlIndex = 0;
char *urls[] = {"http://playerservices.streamtheworld.com/api/livestream-redirect/RADIO538AAC.aac",
                "http://playerservices.streamtheworld.com/api/livestream-redirect/CLASSICFMAAC.aac",
                "http://playerservices.streamtheworld.com/api/livestream-redirect/RADIO10AAC.aac"};

int url_count = 3; // TODO: calculate this

bool radioInitialized = false;

static audio_pipeline_handle_t *pipeline;
static audio_element_handle_t http_stream_reader;
static audio_element_handle_t aac_decoder;
static audio_element_handle_t i2s_stream_writer;

static audio_board_handle_t board_handle;
static audio_event_iface_handle_t evt;
static esp_periph_set_handle_t set;

static bool running = false;
void radio_stop()
{
    if (!running)
    {
        ESP_LOGE(TAG, "radio not running");
        return;
    }
    ESP_LOGI(TAG, "stopping radio");
    running = false;
    pipeline_reset();
}

/**
 * initialized the radio
 * Author: David Stulemeijer
 * Referenced from espressif example projects
 */
void radio_init(void *params)
{
    if (radioInitialized == true)
    {
        vTaskDelete(NULL);
    }
    radio_reset();

    // esp_err_t err = nvs_flash_init();
    // if (err == ESP_ERR_NVS_NO_FREE_PAGES)
    // {
    //     ESP_ERROR_CHECK(nvs_flash_erase());
    //     err = nvs_flash_init();
    // }

    esp_netif_init();
    ESP_LOGI(TAG, "[ * ] amount of stations: %d", url_count);

    esp_log_level_set("*", ESP_LOG_WARN);
    esp_log_level_set(TAG, ESP_LOG_DEBUG);

    ESP_LOGI(TAG, "[ * ] building pipeline");
    pipeline = pipeline_set(radio_config, 3);
    ESP_LOGI(TAG, "pipline is null = %d", pipeline == NULL);

    ESP_LOGI(TAG, "[ * ] get http element ");
    http_stream_reader = audio_pipeline_get_el_by_tag(*pipeline, "http");

    ESP_LOGI(TAG, "[ * ] get aac element ");
    aac_decoder = audio_pipeline_get_el_by_tag(*pipeline, "aac");

    ESP_LOGI(TAG, "[ * ] get i2s writer element ");
    i2s_stream_writer = audio_pipeline_get_el_by_tag(*pipeline, "i2s_out");

    ESP_LOGI(TAG, "[2.6] Set up  uri (http as http_stream, aac as aac decoder, and default output is i2s)");
    audio_element_set_uri(http_stream_reader, urls[currentUrlIndex]);

    ESP_LOGI(TAG, "[ 3 ] Start and wait for Wi-Fi network");
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    set = esp_periph_set_init(&periph_cfg);

    ESP_LOGI(TAG, "[ 4 ] Initialize Touch peripheral");
    audio_board_key_init(set);

    ESP_LOGI(TAG, "[ 5 ] Set up  event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);

    ESP_LOGI(TAG, "[5.1] Listening event from all elements of pipeline");
    audio_pipeline_set_listener(*pipeline, evt);

    ESP_LOGI(TAG, "[5.2] Listening event from peripherals");
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);
    vTaskDelay(100 / portTICK_RATE_MS);

    pipeline_start();
    radioInitialized = true;
    running = true;
    while (running)
    {
        ESP_LOGI(TAG, "Running is %d", running);
        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK)
        {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }
        
        // if music info is recieved
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)aac_decoder && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
        {
            audio_element_info_t music_info = {0};
            audio_element_getinfo(aac_decoder, &music_info);
            audio_element_setinfo(i2s_stream_writer, &music_info);
            i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
            continue;
        }

        // if an error event is recieved
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)http_stream_reader && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (int)msg.data == AEL_STATUS_ERROR_OPEN)
        {
            ESP_LOGW(TAG, "[ * ] stopping stream");
            break;
        }

        if ((msg.source_type == PERIPH_ID_TOUCH || msg.source_type == PERIPH_ID_BUTTON || msg.source_type == PERIPH_ID_ADC_BTN) && (msg.cmd == PERIPH_TOUCH_TAP || msg.cmd == PERIPH_BUTTON_PRESSED || msg.cmd == PERIPH_ADC_BUTTON_PRESSED))
        {

            if ((int)msg.data == get_input_play_id())
                radio_decrease_channel();

            if ((int)msg.data == get_input_set_id())
                radio_increase_channel();

            if ((int)msg.data == get_input_volup_id())
                radio_increase_volume();

            if ((int)msg.data == get_input_voldown_id())
                radio_decrease_volume();
        }
    }
    radioInitialized = false;
    vTaskDelete(NULL);
}

/**
 * converts radio channel to the next defined channel, if possible
 * Author: David Stulemeijer
 */
void radio_increase_channel()
{
    ESP_LOGI(TAG, "[ * ] incremented radio");
    if (currentUrlIndex < url_count - 1)
        currentUrlIndex = currentUrlIndex + 1;
    radio_reset();
    ESP_LOGI(TAG, "[ * ] playing station %d: %s", currentUrlIndex + 1, urls[currentUrlIndex]);
}

/**
 * converts radio channel to the previous defined channel, if possible
 * Author: David Stulemeijer
 */
void radio_decrease_channel()
{
    ESP_LOGI(TAG, "[ * ] decreased radio");
    if (currentUrlIndex > 0)
        currentUrlIndex = currentUrlIndex - 1;
    radio_reset();
    ESP_LOGI(TAG, "[ * ] playing station %d: %s", currentUrlIndex + 1, urls[currentUrlIndex]);
}

void radio_set_channel(void *ptr)
{
    int channelIndex = (int *)ptr;
    ESP_LOGI(TAG, "[ * ] set radio");
    if (channelIndex > -1 && channelIndex < url_count)
    {
        currentUrlIndex = channelIndex;
    }
    radio_reset();
    ESP_LOGI(TAG, "[ * ] playing station %d: %s", currentUrlIndex + 1, urls[currentUrlIndex]);
}

/**
 * increases the radio volume,
 * Author: David Stulemeijer
 */
void radio_increase_volume()
{
    if (radioInitialized == false)
        return;

    ESP_LOGI(TAG, "[ * ] [Vol+] touch tap event");
    if (volume <= 0)
        audio_hal_set_mute(board_handle->audio_hal, false);

    if (volume < 100)
        volume = volume + 10;

    audio_hal_set_volume(board_handle->audio_hal, volume);
    ESP_LOGI(TAG, "[ * ] set volume to %d", volume);
}

/**
 * decreases the radio volume, if 0 is reached the board gets muted
 * Author: David Stulemeijer
 */
void radio_decrease_volume()
{
    if (radioInitialized == false)
        return;

    ESP_LOGI(TAG, "[ * ] [Vol-] touch tap event");
    if (volume > 0)
    {
        volume = volume - 10;
        audio_hal_set_volume(board_handle->audio_hal, volume);
    }
    if (volume <= 0)
    {
        audio_hal_set_mute(board_handle->audio_hal, true);
    }
    ESP_LOGI(TAG, "[ * ] set volume to %d", volume);
}

/**
 * resets the radio if one is initialized
 * Author: David Stulemeijer
 */
void radio_reset()
{
    if (radioInitialized == false)
        return;

    ESP_LOGI(TAG, "[ * ] Reset audio_pipeline");
    audio_pipeline_stop(*pipeline);
    audio_pipeline_wait_for_stop(*pipeline);
    if(aac_decoder != NULL) audio_element_reset_state(aac_decoder);
    if(i2s_stream_writer != NULL) audio_element_reset_state(i2s_stream_writer);
    audio_pipeline_reset_ringbuffer(*pipeline);
    audio_pipeline_reset_items_state(*pipeline);
    audio_element_set_uri(http_stream_reader, urls[currentUrlIndex]);
    audio_pipeline_run(*pipeline);
}