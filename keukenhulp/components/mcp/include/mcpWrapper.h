#ifndef mcpwrapper_h //start of include guard
#define mcpwrapper_h //end of include guard

/*
   mcp23017_gpio_t

   Specifies a group of GPIO pins, either
   group A or group B
*/
typedef enum
{
    GPIOA = 0x00,
    GPIOB = 0x01
} mcp23017_gpio_t;

typedef enum
{
    MCP23017_IODIR = 0x00,
    MCP23017_IPOL = 0x01,
    MCP23017_GPINTEN = 0x02,
    MCP23017_DEFVAL = 0x03,
    MCP23017_INTCON = 0x04,
    MCP23017_IOCON = 0x05,
    MCP23017_GPPU = 0x06,
    MCP23017_INTF = 0x07,
    MCP23017_INTCAP = 0x08,
    MCP23017_GPIO = 0x09,
    MCP23017_OLAT = 0x0A
} mcp23017_reg_t;

void MCP_init();

void MCP_writeToA(int16_t pin);
void MCP_writeToB(int16_t pin);

void MCP_writeToAPin(unsigned char pinnumber, unsigned char value);
void MCP_writeToBPin(unsigned char pinnumber, unsigned char value);


unsigned char MCP_readFromA();
unsigned char MCP_readFromB();



#endif //end of include guard mcpwrapper.h