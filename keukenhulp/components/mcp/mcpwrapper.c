/*
 * Written by Rick Buring and Daan van Donk, Avans Hogeschool
 */

#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "mcpwrapper.h"
#include "smbus.h"

static const char *TAG = "MCP-Wrapper";
smbus_info_t *smbus;

static volatile int16_t current_Abits = 0x00;
static volatile int16_t current_Bbits = 0x00;

/**
 * Converts generic register and group (A/B) to register address
 * @param reg the generic register index
 * @param group the group (A/B) to compute offset
 * @return The register address specified by the parameters
 */
uint8_t AorB(mcp23017_reg_t reg, mcp23017_gpio_t group)
{
    return (group == GPIOA) ? (reg << 1) : (reg << 1) | 1;
}

#define mcp_address 0x20
#define I2C_MASTER_NUM 0

void GPIO_Extender_init()
{
    smbus = smbus_malloc();
    smbus_init(smbus, I2C_MASTER_NUM, mcp_address);
    smbus_set_timeout(smbus, 1000 / portTICK_RATE_MS);
    ESP_LOGI(TAG, "smbus initialized %d", smbus->init);
}

void MCP_init()
{
    GPIO_Extender_init();
}

void MCP_writeToA(int16_t pin)
{
    uint8_t r = AorB(MCP23017_IODIR, GPIOA);

    smbus_write_word(smbus, r, 0x00);

    r = AorB(MCP23017_GPIO, GPIOA);
    smbus_write_word(smbus, r, pin);
}

void MCP_writeToB(int16_t pin)
{

    uint8_t r = AorB(MCP23017_IODIR, GPIOB);
    smbus_write_word(smbus, r, 0x00);
    r = AorB(MCP23017_GPIO, GPIOB);
    smbus_write_word(smbus, r, pin);
}

void MCP_writeToAPin(unsigned char pinnumber, unsigned char value)
{
    if (pinnumber > 7)
    {
        return;
    }

    if (value > 0)
    {
        value = (1 << pinnumber);
        int testbool = current_Abits & value;
        if (testbool == 0)
        {
            current_Abits = current_Abits | value;
        }
    }
    else
    {
        value = ~(1 << pinnumber);

        current_Abits = current_Abits & value;
    }

    uint8_t r = AorB(MCP23017_IODIR, GPIOA);
    smbus_write_word(smbus, r, 0x00);

    r = AorB(MCP23017_GPIO, GPIOA);

    smbus_write_word(smbus, r, current_Abits);
}

void MCP_writeToBPin(unsigned char pinnumber, unsigned char value)
{
    if (pinnumber > 7)
    {
        return;
    }

    if (value > 0)
    {
        value = (1 << pinnumber);
        int testbool = current_Bbits & value;
        if (testbool == 0)
        {
            current_Bbits = current_Bbits | value;
        }
    }
    else
    {
        value = ~(1 << pinnumber);

        current_Bbits = current_Bbits & value;
    }

    uint8_t r = AorB(MCP23017_IODIR, GPIOB);

    smbus_write_word(smbus, r, 0x00);

    r = AorB(MCP23017_GPIO, GPIOB);

    smbus_write_word(smbus, r, current_Bbits);
}

uint8_t MCP_readFromA()
{
    uint8_t data[1];

    uint8_t r = AorB(MCP23017_IODIR, GPIOA);

    smbus_write_word(smbus, r, 0xFD);

    r = AorB(MCP23017_GPIO, GPIOA);

    smbus_read_byte(smbus, r, data);

    return data[0];
}

uint8_t MCP_readFromB()
{
    uint8_t data[1];

    uint8_t r = AorB(MCP23017_IODIR, GPIOB);

    smbus_write_word(smbus, r, 0xFD);

    r = AorB(MCP23017_GPIO, GPIOB);

    smbus_read_byte(smbus, r, data);

    return data[0];
}