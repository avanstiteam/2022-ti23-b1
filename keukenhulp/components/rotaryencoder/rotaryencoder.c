/*
* Written by Rick Buring and Daan van Donk, Avans Hogeschool
*/


#include <stdio.h>
#include <stdbool.h>
#include "rotaryencoder.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "qwiic_twist.h"
#include "esp_log.h"

/*
element 0 = onClick
element 1 = onLeftTwist
element 2 = onRightTwist
*/
static const char *TAG = "Rotary";

void (*onclicklisteners[3])() = {NULL, NULL, NULL};

#define QWIIC_TWIST_ADDRESS 0x3F

qwiic_twist_t pQwiic_twist;

/**
 * @brief Set the callback for a certain action
 * 
 * @param functionPointer The method to call back ti
 * @param action The action when to raise callback event
 */
void rotary_setonaction(void *functionPointer, encoder_actions action)
{
    onclicklisteners[action] = functionPointer;
}

/**
 * @brief When to rotary encoder is twisted
 * 
 * @param num -1 is to the right, 1 is to the left
 */
void rotary_onMoved(int16_t num)
{
    ESP_LOGI(TAG, "moved amound: %d", num);
    if (num > 0 && onclicklisteners[2] != NULL)
    {
        onclicklisteners[2]();
    }
    else if (num < 0 && onclicklisteners[2] != NULL)
    {
        onclicklisteners[1]();
    }
    qwiic_twist_set_color(&pQwiic_twist, 157, 197, 255);
}

/**
 * @brief When the rotary encoder is clicked
 * 
 */
void rotary_onClicked()
{
    ESP_LOGI(TAG, "clicked");
    if (onclicklisteners[2] != NULL)
        onclicklisteners[0]();
}

/**
 * @brief Init for the i2c link of the rotary encoder
 * 
 */
static void config_qwicc_twist_init(void)
{
    pQwiic_twist.i2c_addr = QWIIC_TWIST_ADDRESS;
    pQwiic_twist.port = 0;
    pQwiic_twist.onButtonClicked = rotary_onClicked;
    pQwiic_twist.onMoved = rotary_onMoved;

    qwiic_twist_init(&pQwiic_twist);
}

/**
 * @brief Init for the rotary encoder
 * 
 */
void rotary_init()
{
    config_qwicc_twist_init();
    qwiic_twist_set_color(&pQwiic_twist, 157, 197, 255);
    qwiic_twist_start_task(&pQwiic_twist);
}