#ifndef roteryencoder_h //start of include guard
#define roteryencoder_h
#include <stdio.h>
typedef enum {
    ONCLICK = 0,
    ONTWISTLEFT = 1,
    ONTWISTRIGHT = 2,
} encoder_actions;


void rotary_init();
void rotary_setcolor(uint8_t r, uint8_t g, uint8_t b);
void rotary_setonaction(void * functionPointer, encoder_actions action);

#endif //end of include guard rotaryencoder.h