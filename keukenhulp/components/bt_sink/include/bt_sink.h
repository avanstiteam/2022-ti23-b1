#ifndef SINK_H
#define SINK_H

void sink_init();
void sink_stop();
void sink_pause();
void sink_next();
void sink_previous();

#endif