#ifndef component_timer_h //include guard
#define component_timer_h

void timer_start(int seconds, void(*fun), void *args);
void timer_stop();
char* timer_timeLeft();

#endif //end of include guard component_timer.h