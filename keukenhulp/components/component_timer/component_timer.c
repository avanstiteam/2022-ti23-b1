#include <stdio.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "component_timer.h"
#include "freertos/timers.h"
#include "sevenSegmentManager.h"

static const char *TAG = "TIMER";

bool timerActive = false;
int duration = 0;
char timeLeft[30];
TimerHandle_t timer;

static void (*_ptr)(void *) = NULL;
static void *_args = NULL;

/**
 * returns formatted char* of the timeLeft variable
 * Author: David Stulemeijer
 */
char *timer_timeLeft()
{

    int hours = duration / 3600;
    int minutes = duration % 3600 / 60;
    int seconds = duration % 3500 % 60;

    sprintf(timeLeft, "%d:%d:%d", hours, minutes, seconds);
    return timeLeft;
}

/**
 * Callback for timer, activated every time the timer is hit
 * @param pxTimer is the triggered timer
 * Author: David Stulemeijer
 */
void vTimerCallback(TimerHandle_t pxTimer)
{
    if (pxTimer == NULL)
    {
        ESP_LOGE(TAG, "lost the timer...returning");
        return;
    }

    duration--;

    int hours = duration / 3600;
    int minutes = duration % 3600 / 60;
    int seconds = duration % 3500 % 60;

    seg7_setTimeRemaining(hours, minutes, seconds);

    ESP_LOGI(TAG, "%s", timer_timeLeft());
    if (duration == 0)
    {
        if (_ptr != NULL)
        {
            _ptr(_args);
        }
        timer_stop();
        ESP_LOGI(TAG, "timer stopped");
    }
}

/**
 * init function for the timer
 * @param seconds is the amount of seconds a timer should take to call the final callback
 * @param fun is the function pointer to be triggert on completion of the timer
 * @param args is optional for argument of function
 * Author: David Stulemeijer
 */
void timer_start(int seconds, void(*fun), void *args)
{
    if (timerActive == true)
    {
        ESP_LOGW(TAG, "an attempt was made to start a while another is in progress. \nplease stop the first Timer before creating another");
        return;
    }

    if (seconds < 1)
    {
        return;
    }

    duration = seconds;
    _ptr = fun;
    args = args;

    timer = xTimerCreate("Timer",                   // Just a text name, not used by the kernel.
                         (1000 / portTICK_RATE_MS), //   // The timer period in ticks.
                         pdTRUE,                    // The timers will auto-reload themselves when they expire.
                         (void *)1,                 // Assign each timer a unique id equal to its array index.
                         vTimerCallback             // Each timer calls the same callback when it expires.
    );
    if (timer == NULL)
    {
        ESP_LOGE(TAG, "failed to create timer");
    }
    else
    {
        if (xTimerStart(timer, 0) != pdPASS)
        {
            ESP_LOGE(TAG, "failed to activate timer");
            // The timer could not be set into the Active state.
        }
    }
    timerActive = true;
    seg7_displayTimer(true);
};

/**
 * Function to stop the timer
 * Author: David Stulemeijer
 */
void timer_stop()
{
    seg7_displayTimer(false);
    xTimerStop(timer, 0);
    timerActive = false;
};