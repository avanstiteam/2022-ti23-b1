#ifndef LCD_wrapper_h //start of include guard
#define LCD_wrapper_h

void LCD_init();
void LCD_clear();
void LCD_write(char string[], int row);

#endif //end of include guard LCD_wrapper.h