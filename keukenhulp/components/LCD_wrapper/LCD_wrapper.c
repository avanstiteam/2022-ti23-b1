#include <stdio.h>
#include "LCD_wrapper.h"
#include "LCD.h"
#include "smbus.h"

#define I2C_MASTER_NUM 0
#define LCD_ADDRESS_NUMBER 0x27
#define LCD_NUM_ROWS 4
#define LCD_NUM_COLUMNS 32
#define LCD_NUM_VISIBLE_COLUMNS 20

#define SIZEOF(arr) sizeof(arr) / sizeof(arr[0])

i2c_lcd1602_info_t *lcd_info = NULL;

void LCD_init() {
    i2c_port_t i2c_port = I2C_MASTER_NUM;
    uint8_t lcd_address = LCD_ADDRESS_NUMBER;
    smbus_info_t * smbus_info = smbus_malloc();
    smbus_init(smbus_info, i2c_port, lcd_address);
    smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS);

    lcd_info = i2c_lcd1602_malloc();
    i2c_lcd1602_init(lcd_info, smbus_info, true, LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VISIBLE_COLUMNS);

    i2c_lcd1602_reset(lcd_info);

    //draw splash screen
    LCD_write("Keukenhulp", 1);
}

void LCD_clear()
{
    i2c_lcd1602_clear(lcd_info);
}

void LCD_write(char string[], int row) {
    i2c_lcd1602_move_cursor(lcd_info, 0, row);
    for(int i = 0; i < 20; i++)  
    {
        if(!string[i]) {
            break;
        }

        i2c_lcd1602_write_char(lcd_info, string[i]);
    }
}
