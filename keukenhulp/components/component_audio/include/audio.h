#ifndef AUDIO_H
#define AUDIO_H

#include <esp_err.h>

#define TEST_SOUND "/sdcard/test.mp3"

void stop_audio_pipeline();
esp_err_t audio_init_pipeline();
esp_err_t audio_playSound(const char*);

#endif