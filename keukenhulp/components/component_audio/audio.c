#include "audio.h"

#include <esp_err.h>

#include "audioPipeLine.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "fatfs_stream.h"
#include "i2s_stream.h"
#include "mp3_decoder.h"

#include <string.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "sdkconfig.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "fatfs_stream.h"
#include "i2s_stream.h"
#include "mp3_decoder.h"
#include "esp_peripherals.h"
#include "periph_sdcard.h"
#include "board.h"

static const char *TAG = "PLAY_SDCARD_MUSIC";

static audio_pipeline_handle_t *pipeline;
static audio_element_handle_t fatfs_stream_reader, i2s_stream_writer, music_decoder;
static esp_periph_set_handle_t set;
static audio_event_iface_handle_t evt;

bool playingAudio = false;
static bool initialized = false;
esp_err_t audio_init_pipeline()
{

    if(!initialized){
        initialized = true;
        ESP_LOGI(TAG, "[ 0 ] get periph handle");
        esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
        set = esp_periph_set_init(&periph_cfg);
        ESP_LOGI(TAG, "[ 1 ] Mount sdcard");
        audio_board_sdcard_init(set, SD_MODE_1_LINE);
    }

    pipeline = pipeline_set(audio_config, 3);
    // mem_assert(pipeline);

    ESP_LOGI(TAG, "[3.1] Get fatfs stream to read data from sdcard");

    fatfs_stream_reader = audio_pipeline_get_el_by_tag(*pipeline, "fatfs");
    if (fatfs_stream_reader == NULL)
    {
        ESP_LOGE(TAG, "[3.2.1] i2s stream writer null");
    }

    ESP_LOGI(TAG, "[3.2] Get i2s stream to write data to codec chip");
    i2s_stream_writer = audio_pipeline_get_el_by_tag(*pipeline, "i2s_out");
    if (i2s_stream_writer == NULL)
    {
        ESP_LOGE(TAG, "[3.2.1] i2s stream writer null");
    }

    ESP_LOGI(TAG, "[3.3] Get mp3 decoder");
    music_decoder = audio_pipeline_get_el_by_tag(*pipeline, "music");
    if (music_decoder == NULL)
    {
        ESP_LOGE(TAG, "[3.2.1] mp3 decoder = null");
    }

    ESP_LOGI(TAG, "[3.6] Set up uri");
    audio_element_set_uri(fatfs_stream_reader, "");

    ESP_LOGI(TAG, "[ 4 ] Set up  event listener");
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    evt = audio_event_iface_init(&evt_cfg);

    ESP_LOGI(TAG, "[4.1] Listening event from all elements of pipeline");
    audio_pipeline_set_listener(*pipeline, evt);

    ESP_LOGI(TAG, "[4.2] Listening event from peripherals");
    audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), evt);

    ESP_LOGI(TAG, "[ 5 ] Start audio_pipeline");
    pipeline_start();

    return ESP_OK;
}

esp_err_t audio_playSound(const char *filename)
{
    playingAudio = true;

    ESP_LOGI(TAG, "[ * ] Set up uri: %s", filename);
    audio_element_set_uri(fatfs_stream_reader, filename);
    pipeline_reset();
    pipeline_start();

    while (1)
    {

        if (!playingAudio)
            break;

        audio_event_iface_msg_t msg;
        esp_err_t ret = audio_event_iface_listen(evt, &msg, portMAX_DELAY);
        if (ret != ESP_OK)
        {
            ESP_LOGE(TAG, "[ * ] Event interface error : %d", ret);
            continue;
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)music_decoder && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
        {
            audio_element_info_t music_info = {0};
            audio_element_getinfo(music_decoder, &music_info);

            ESP_LOGI(TAG, "[ * ] Receive music info from mp3 decoder, sample_rates=%d, bits=%d, ch=%d",
                     music_info.sample_rates, music_info.bits, music_info.channels);

            audio_element_setinfo(i2s_stream_writer, &music_info);
            i2s_stream_set_clk(i2s_stream_writer, music_info.sample_rates, music_info.bits, music_info.channels);
            continue;
        }

        /* Stop when the last pipeline element (i2s_stream_writer in this case) receives stop event */
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)i2s_stream_writer && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (((int)msg.data == AEL_STATUS_STATE_STOPPED) || ((int)msg.data == AEL_STATUS_STATE_FINISHED)))
        {
            ESP_LOGW(TAG, "[ * ] Stop event received");
            break;
        }
    }

    return ESP_OK;
}

void stop_audio_pipeline()
{
    if (playingAudio)
    {
        playingAudio = false;
    }
}