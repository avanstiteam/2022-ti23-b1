#ifndef component_menu_h
#define component_menu_h

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define MAX_MENU_KEY 4
#define MAX_CHAR_COUNT 20
#define LCD_MAX_LINES 4
#define LCD_MAX_WIDTH 20

/* Define indices for the keys in the array of new IDs */
#define MENU_KEY_ESC 0
#define MENU_KEY_OK 1
#define MENU_KEY_LEFT 2
#define MENU_KEY_RIGHT 3

/* Define the IDs for all menu items */
#define MENU_MAX_LINES 10


#define COUNT(x)  (sizeof(x) / sizeof((x)[0]))

/* Define struct to hold data for a single menu item */
typedef struct menu MENU_WINDOW;
typedef struct menu_line MENU_LINE;
 
typedef enum alignment_default ALIGNMENT_TYPE;



struct menu_line {
	char* text;
	void (*functionPointer)(void*);
	void* param;
	bool centerText;
};

// Defenition of the menu screen 
struct menu {
	char* title;
	MENU_LINE lines[MENU_MAX_LINES];
	bool hidePointer;
	int totalsubmenus;
};

void menu_init(MENU_WINDOW *input);
void menu_move_window(void*);
void setup_menu_controls();

#endif //end of include guard component_menu.h