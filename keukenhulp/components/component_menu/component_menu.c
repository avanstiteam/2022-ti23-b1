#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "esp_log.h"
#include "component_menu.h"
#include "LCD_wrapper.h"
#include "rotaryencoder.h"

static const char *TAG = "MENU";

int currentLine = 0;
int currentPage = 0;

MENU_WINDOW *currentMenu;

static void centerText(char *text, char *content)
{

	int strSize = (MAX_CHAR_COUNT - strlen(content)) / 2;
	strcpy(text, "");
	for (size_t i = 0; i < strSize; i++)
	{
		strcat(text, " ");
	}
	strcat(text, content);
}

void menu_printItemWindow()
{
	LCD_clear();

	if (currentMenu->title != NULL)
	{
		char text[20];
		centerText(text, currentMenu->title);
		LCD_write(text, 0);
	}

	for (int i = 0; i < 3; i++)
	{
		int index = i + (currentPage * 3);

		if (index < (currentMenu->totalsubmenus))
		{
			MENU_LINE *line = &currentMenu->lines[index];
			if (NULL == line)
			{
				break;
			}
			if (i == currentLine && !currentMenu->hidePointer)
			{
				char result[20];
				sprintf(result, "> %s", line->text);
				LCD_write(result, (i + 1));
			}
			else
			{
				if (line->centerText)
				{
					char text[20];
					centerText(text, line->text);
					LCD_write(text, (i + 1));
				}
				else
					LCD_write(line->text, (i + 1));
			}
		}
	}
	fflush(stdout);
}

void menu_onLeftTwist()
{
	if (currentLine - 1 < 0)
	{
		if (currentPage <= 0)
		{
			currentPage = 0;
			return;
		}
		else
		{
			currentPage--;
		}

		currentLine = 2;
	}
	else
	{
		currentLine--;
	}

	menu_printItemWindow();
}

void menu_onRightTwist()
{
	if (currentPage * 3 + currentLine + 2 > currentMenu->totalsubmenus)
	{
		return;
	}
	else if (currentLine + 1 <= 2)
	{
		currentLine++;
	}
	else
	{
		currentPage++;
		currentLine = 0;
	}

	menu_printItemWindow();
}

void menu_onClick()
{	
	ESP_LOGI(TAG, "onClick was called");

	int menuIndex = currentPage * 3 + currentLine;
	ESP_LOGI(TAG,"current menu title is %s, selected index is %d", currentMenu->title, menuIndex);
	fflush(stdout);

	MENU_LINE *selected = &currentMenu->lines[menuIndex];
	if (NULL == selected)
	{
		ESP_LOGW(TAG, "clicked line is null");
		return;
	}
	if (NULL != &selected->functionPointer)
	{
		(*selected->functionPointer)(selected->param);
	}
}

void menu_move_window(void *window)
{
	ESP_LOGI(TAG, "moving to %s", ((MENU_WINDOW *)window)->title);
	currentMenu = (MENU_WINDOW *)window;
	currentPage = 0;
	currentLine = 0;
	menu_printItemWindow();
}

void menu_init(MENU_WINDOW *input)
{
	currentMenu = input;
	setup_menu_controls();
}

void setup_menu_controls()
{
	menu_printItemWindow();
	rotary_setonaction(menu_onLeftTwist, ONTWISTLEFT);
	rotary_setonaction(menu_onRightTwist, ONTWISTRIGHT);
	rotary_setonaction(menu_onClick, ONCLICK);
}
