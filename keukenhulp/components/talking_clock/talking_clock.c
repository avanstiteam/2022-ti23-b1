#include "talking_clock.h"
#include <esp_log.h>
#include <esp_err.h>
#include <time.h>
#include <math.h>
#include "audio.h"

#include "esp_timer.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_attr.h"

#define QUEUE_ADD(toAdd) \
	if (xQueueSend(talking_clock_queue, (toAdd), portMAX_DELAY) != pdPASS) { \
		ESP_LOGE(TAG, "Cannot queue data"); \
		return ESP_FAIL; \
	}

static const char* TAG = "TALKING_CLOCK";
static TimerHandle_t clockTimer;
static int nextTimerInterval = 1000*60*15;

static void talking_clock_timer_callback(TimerHandle_t xTimer);
static struct tm getCurrentTime();

esp_err_t talking_clock_init() {
	
	// Initialize queue
	ESP_LOGI(TAG, "Creating FreeRTOS queue for talking clock");
	talking_clock_queue = xQueueCreate(10, sizeof(char[MAX_PATH_LENGTH]));
	
	if (talking_clock_queue == NULL) {
		ESP_LOGE(TAG, "Error creating queue");
		return ESP_FAIL;
	}
	return ESP_OK;
}

esp_err_t talking_clock_fill_queue() {
	struct tm timeinfo = getCurrentTime();
	
	// Reset queue
	if (xQueueReset(talking_clock_queue) != pdPASS) {
		ESP_LOGE(TAG, "Cannot reset queue\n");
		return ESP_FAIL;
	}
	
	// setup variables for queue
	int quarterHour = (int) round(timeinfo.tm_min / 15.0); // rounded integer division to get time in quarter hours
	int hourIndex = timeinfo.tm_hour;  // the current hour
	if (quarterHour >= 2) hourIndex++; // if it is half past X or quarter to X the hour needs to be one lower
	hourIndex = (hourIndex + 12) % 12; // hourIndex needs to be between 0 and 12, if it is negative, it needs to go above 12, then wrapped around if nececary
	char* hour = talking_clock_digits[hourIndex];

	// Fill queue
	QUEUE_ADD(CLK_INTRO);

	 if (quarterHour == 1) { // quarter past
		QUEUE_ADD(CLK_QUARTER);
		QUEUE_ADD(CLK_AFTER);
	}
	if (quarterHour == 2) { // half past
		QUEUE_ADD(CLK_HALF);
	}
	if (quarterHour == 3) { // quarter to
		QUEUE_ADD(CLK_QUARTER);
		QUEUE_ADD(CLK_BEFORE);
	}
	QUEUE_ADD(hour);
	if (quarterHour == 0 || quarterHour == 4) { // full hour
		QUEUE_ADD(CLK_HOUR);
	}
	
	ESP_LOGI(TAG, "Queue filled with %d items", uxQueueMessagesWaiting(talking_clock_queue));
	
	return ESP_OK;
}

void ReadOutTime() {
	ESP_LOGI(TAG, "[ * ] starting pipeline");
    if (audio_init_pipeline() != ESP_OK) {
		ESP_LOGE(TAG, "Failed to initialize pipeline");
		return;
	}

	char buffer[MAX_PATH_LENGTH] = "";
	while (1) {
		if (uxQueueMessagesWaiting(talking_clock_queue) <= 0) {
			break;
		}
		xQueueReceive(talking_clock_queue, buffer, portMAX_DELAY);
		if (audio_playSound(buffer) != ESP_OK) {
			ESP_LOGE(TAG, "Failed to play sound");
			break;
		}
		vTaskDelay(1500 / portTICK_RATE_MS);
	}
	

	ESP_LOGI(TAG, "[ * ] stopping pipeline");
	stop_audio_pipeline();
}

esp_err_t talking_clock_say_current_time() {
	if (talking_clock_fill_queue() != ESP_OK) {
		ESP_LOGE(TAG, "Failed to fill queue with current time");
		return ESP_FAIL;
	}
	ReadOutTime();
	return ESP_OK;
}

void talking_clock_setTaskDelay(clockTimerDelay_t taskDelay) {
	struct tm timeinfo = getCurrentTime();

	int nextInterval = 0;
	if (taskDelay == quarter) {
		nextTimerInterval = 1000*60*15;
		nextInterval = ((60 - timeinfo.tm_min) % 15);
	} else if (taskDelay == halfHour) {
		nextTimerInterval = 1000*60*30;
		nextInterval = ((60 - timeinfo.tm_min) % 30);
	} else if (taskDelay == fullHour) {
		nextTimerInterval = 1000*60*60;
		nextInterval = ((60 - timeinfo.tm_min) % 60);
	}

	nextTimerInterval = 2 * 1000 * 60;

	if (nextInterval != 0) {
		ESP_LOGI(TAG, "nextInterval: %d minutes", nextInterval);
		// xTimerChangePeriod(clockTimer, pdMS_TO_TICKS((1000 * 60 * nextInterval) - timeinfo.tm_sec), portMAX_DELAY);
		xTimerChangePeriod(clockTimer, pdMS_TO_TICKS(15000), portMAX_DELAY);
	} else {
		ESP_LOGI(TAG, "nextInterval: %d milliseconds", nextTimerInterval);
		xTimerChangePeriod(clockTimer, pdMS_TO_TICKS(nextTimerInterval), portMAX_DELAY);
	}
}

static struct tm getCurrentTime() {
	time_t now;
    time(&now);
	
    struct tm timeinfo;
	localtime_r(&now, &timeinfo);

	char strftime_buf[64];
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);

    ESP_LOGI(TAG, "The current date/time in Amsterdam is: %s", strftime_buf);

	return timeinfo;
}

static void talking_clock_talk_task(void *arg) {
	talking_clock_say_current_time();

	xTimerChangePeriod(clockTimer, pdMS_TO_TICKS(nextTimerInterval), portMAX_DELAY);

	vTaskDelete(NULL);
}

static void talking_clock_timer_callback(TimerHandle_t xTimer) {
	xTaskCreate(talking_clock_talk_task, "Talking Clock Timer Callback", 8192, NULL, 1, NULL);
}