#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"

#define MAX_PATH_LENGTH 30

esp_err_t talking_clock_init();
esp_err_t talking_clock_fill_queue();
esp_err_t talking_clock_say_current_time();
void ReadOutTime();
void talking_clock_setTaskDelay(clockTimerDelay_t);

typedef enum {
    fullHour = 0,
    quarter = 1,
    halfHour = 2
} clockTimerDelay_t;

#define CLK_INTRO   "/sdcard/clock/hetisnu.mp3"
#define CLK_BEFORE  "/sdcard/clock/voor.mp3"
#define CLK_AFTER   "/sdcard/clock/over.mp3"
#define CLK_QUARTER "/sdcard/clock/kwart.mp3"
#define CLK_HALF    "/sdcard/clock/half.mp3"
#define CLK_HOUR    "/sdcard/clock/uur.mp3"

/*
 * A list of the full hour times
 * twelve is on top because this can be used in a modulo 12, otherwise there needed to be a filler null pointer
 */
static char talking_clock_digits[12][MAX_PATH_LENGTH] = {
    "/sdcard/clock/twaalf.mp3",
    "/sdcard/clock/een.mp3",
    "/sdcard/clock/twee.mp3",
    "/sdcard/clock/drie.mp3",
    "/sdcard/clock/vier.mp3",
    "/sdcard/clock/vijf.mp3",
    "/sdcard/clock/zes.mp3",
    "/sdcard/clock/zeven.mp3",
    "/sdcard/clock/acht.mp3",
    "/sdcard/clock/negen.mp3",
    "/sdcard/clock/tien.mp3",
    "/sdcard/clock/elf.mp3"
    };

QueueHandle_t talking_clock_queue;