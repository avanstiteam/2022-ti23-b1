#ifndef SNTP_H //start of include guard
#define SNTP_H

#include "esp_sntp.h"


#ifdef __cplusplus
extern "C" {
#endif


void sntp_sync(sntp_sync_time_cb_t callback);
void syncTime_init();

#ifdef __cplusplus
}
#endif

#endif  //end of include guard SMBUS_H
