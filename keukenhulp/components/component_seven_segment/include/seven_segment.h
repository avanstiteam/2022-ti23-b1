#ifndef seven_segment_H //start of include guard
#define seven_segment_H

#include <stdio.h>
#include <stdbool.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

typedef struct {
    bool order;
    char data;
} ShiftOutData_T;

void seg7_init(int cs, int din, int clk);

void seg7_displayString(char[]);

void seg7_displayTest();

void seg7_setdisplay(unsigned char display, char value);
void seg7_displayTime(int h, int m, int s);

#endif //end include guard seven_segment.H