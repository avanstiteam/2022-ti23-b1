#ifndef sevenSegmentManager_h

#define sevenSegmentManager_h
#include <stdbool.h>

void seg7_setCurrentTime(int h, int m, int s);
void seg7_setTimeRemaining(int h, int m, int s);
void seg7_displayTimer(bool set);


#endif //end include guard sevenSegmentManager

