/*
* Made by Rick Buring and Daan van Donk, Avans Hogeschool
* Helpful documentation which was used:
* http://www.whatimade.today/programming-an-8-digit-7-segment-display-the-easy-way-using-a-max7219/
*/

#include <stdio.h>
#include "mcpWrapper.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "seven_segment.h"

// The pins of 8x7seg
int MAX7219_CS;
int MAX7219_DIN;
int MAX7219_CLK;

// The bytes that set certain settings of the 8x7seg
const unsigned char INTENSITY = 0x0A;
const unsigned char SCANLIMIT = 0x0B;
const unsigned char SHUTDOWN = 0x0C;
const unsigned char DISPLAYTEST = 0x0F;
const unsigned char DECODE = 9;

SemaphoreHandle_t sem;

/**
 * Shift the array of data
 * @param sod The shift out data, bool for order (Left to right or right to left)
 */
void shiftOutTask(ShiftOutData_T sod)
{
    for (int i = 0; i < 8; i++)
    {
        bool output = false;
        // Left to right
        if (sod.order)
        {
            output = sod.data & 0b10000000;
            sod.data = sod.data << 1;
        }

        // Right to left
        else
        {
            output = sod.data & 0b00000001;
            sod.data = sod.data >> 1;
        }

        MCP_writeToBPin(MAX7219_DIN, output);
        MCP_writeToBPin(MAX7219_CLK, 1);
        vTaskDelay(1 / portTICK_PERIOD_MS);
        MCP_writeToBPin(MAX7219_CLK, 0);
        vTaskDelay(1 / portTICK_PERIOD_MS);
    }
    // vTaskDelete(NULL);
}

/**
 * Shift the array of data
 * @param order for order (true is left to right or false is right to left)
 * @param data, the data you want to send to 7 segment
 */
void shiftOut(bool order, char data)
{
    ShiftOutData_T sod = {order, data};
    shiftOutTask(sod);
}

/**
 * Set a register to a value
 * @param reg the register
 * @param value the value you want the register set to
 */
void set_register(char reg, char value)
{
    // Write to low
    MCP_writeToBPin(MAX7219_CS, 0x00);
    shiftOut(true, reg);
    shiftOut(true, value);
    // Write to high
    MCP_writeToBPin(MAX7219_CS, 0x01);
}

/** 
 * @param stringValue the chars you want to send to 8x7seg, left to right
 */
void seg7_displayString(char stringValue[])
{
    // Write chars to indiviuele 7segment displays
    int j = 0;
    for (int i = 8; i > 0; i--)
    {
        set_register(i, stringValue[j]);
        j++;
    }
}

/**
 * @brief display the time on 8x7segment
 * 
 * @param h display hours
 * @param m display minutes
 * @param s display seconds
 */
void seg7_displayTime(int h, int m, int s)
{
    set_register(8, h/10);
    set_register(7, h%10);
    set_register(6, 0b01111111);
    set_register(5, m/10);
    set_register(4, m%10);
    set_register(3, 0b01111111);
    set_register(2, s/10);
    set_register(1, s%10);
}


/**
 * @brief set a single 7segment display
 * 
 * @param display which 7segment display, 1 through 8.
 * @param value what you want to display on the 7segment
 */
void seg7_setdisplay(unsigned char display, char value)
{
    if(display > 8 || display < 1)
    {
        return;
    }
    set_register(display, value);
}

/**
 * @brief Runs the build in display test
 * 
 */
void seg7_displayTest()
{
    set_register(SHUTDOWN, 0x00);
    set_register(DISPLAYTEST, 0x00);
    set_register(INTENSITY, 0x0D);
}

/**
 * @brief Inits the pins of the 8x7seg
 * 
 * @param cs The chip select pin on the board
 * @param din The digital input pin on the board
 * @param clk The clock pin on the board
 */
void seg7_init(int cs, int din, int clk)
{
    MAX7219_CLK = clk;
    MAX7219_DIN = din;
    MAX7219_CS = cs;

    sem = xSemaphoreCreateBinary();

    set_register(SHUTDOWN, 0x01);
    set_register(SCANLIMIT, 7); // limit to 8 digits
    
    set_register(DECODE, 0xff); // decode all digits
}
