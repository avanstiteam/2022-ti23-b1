#include "sevenSegmentManager.h"
#include "seven_segment.h"

static bool displayTimerBool = false;

void seg7_setCurrentTime(int h, int m, int s)
{
    if (!displayTimerBool)
    {
        seg7_displayTime(h, m, s);
    }
}

void seg7_setTimeRemaining(int h, int m, int s)
{
    if (displayTimerBool)
    {
        seg7_displayTime(h, m, s);
    }
}

void seg7_displayTimer(bool set)
{
    displayTimerBool = set;
}