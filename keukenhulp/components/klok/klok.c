#include <stdio.h>
#include "klok.h"

#include <string.h>
#include <sys/time.h>
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "sevenSegmentManager.h"

static const char *TAG = "CLOCK";

static TimerHandle_t timer_1_sec;

void timer_1_sec_callback(TimerHandle_t xTimer)
{
    // Print current time to the screen
    time_t now;
    struct tm timeinfo;
    time(&now);

    char strftime_buf[20];
    localtime_r(&now, &timeinfo);
    sprintf(&strftime_buf[0], "%02d-%02d-%02d\n", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
    // i2c_lcd1602_move_cursor(lcd_info, 0, 1);

    // displayString(strftime_buf);
    seg7_setCurrentTime(timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);
}

TimerHandle_t getTimer(){
    return timer_1_sec_callback;
}

void klok_init(){
    // Initialize 1 second timer to display the time
    int id = 1;
    timer_1_sec = xTimerCreate("MyTimer", pdMS_TO_TICKS(1000), pdTRUE, (void *)id, &timer_1_sec_callback);
    if (xTimerStart(timer_1_sec, 10) != pdPASS)
    {
        ESP_LOGE(TAG, "Cannot start 1 second timer");
    }
}
