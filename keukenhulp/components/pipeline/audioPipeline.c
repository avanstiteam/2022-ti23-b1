#include "audioPipeline.h"

static const char *TAG = "AUDIO_PIPE";

const char *radio_config[3] = {"http", "aac", "i2s_out"};
const char *audio_config[3] = {"fatfs", "music", "i2s_out"};
const char *commands_config[3] = {"i2s_in", "resample", "raw"};
const char *bt_config[2] = {"bt", "i2s_out"};

static audio_pipeline_handle_t pipeline;

static bool isInitialized = false;
static bool hasPipeline = false;
static bool isRunning = false;

esp_err_t pipeline_start()
{
    if (isRunning)
    {
        ESP_LOGE(TAG, "pipline already running!");
        return ESP_FAIL;
    }
    if (!isInitialized || !hasPipeline)
    {
        ESP_LOGE(TAG, "pipeline not starter or initialized");
        return ESP_FAIL;
    }

    ESP_LOGI(TAG, "[ 6 ] Start audio_pipeline");
    esp_err_t err = audio_pipeline_run(pipeline);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "failed to start pipline");
        return err;
    }
    isRunning = true;
    return ESP_OK;
}

esp_err_t pipeline_init()
{

    ESP_LOGI(TAG, "[2.0] Create audio pipeline for playback");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);

    // internet radio
    ESP_LOGI(TAG, "[2.8] Create i2s stream to write data to codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    audio_element_handle_t i2s_stream_writer = i2s_stream_init(&i2s_cfg);
    audio_pipeline_register(pipeline, i2s_stream_writer, "i2s_out");

    ESP_LOGI(TAG, "[2.1] Create http stream to read data");
    http_stream_cfg_t http_cfg = HTTP_STREAM_CFG_DEFAULT();
    http_cfg.type = AUDIO_STREAM_READER;
    audio_element_handle_t http_stream_reader = http_stream_init(&http_cfg);
    audio_pipeline_register(pipeline, http_stream_reader, "http");

    ESP_LOGI(TAG, "[2.5] Create aac decoder to decode aac file");
    aac_decoder_cfg_t aac_cfg = DEFAULT_AAC_DECODER_CONFIG();
    audio_element_handle_t aac_decoder = aac_decoder_init(&aac_cfg);
    audio_pipeline_register(pipeline, aac_decoder, "aac");
    // end internet radio

    // start audio commands
    ESP_LOGI(TAG, "[2.3] Create i2s stream reader");
    i2s_stream_cfg_t i2s_cfg_command = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg_command.type = AUDIO_STREAM_READER;
    i2s_cfg_command.i2s_config.sample_rate = AUDIO_SAMPLE_RATE;
    audio_element_handle_t i2s_stream_reader = i2s_stream_init(&i2s_cfg_command);
    audio_pipeline_register(pipeline, i2s_stream_reader, "i2s_in");

    ESP_LOGI(TAG, "[2.4] Create filter");
    rsp_filter_cfg_t rsp_cfg = DEFAULT_RESAMPLE_FILTER_CONFIG();
    rsp_cfg.src_rate = AUDIO_SAMPLE_RATE;
    rsp_cfg.src_ch = 2;
    rsp_cfg.dest_rate = GOERTZEL_SAMPLE_RATE_HZ;
    rsp_cfg.dest_ch = 1;
    audio_element_handle_t resample_filter = rsp_filter_init(&rsp_cfg);
    audio_pipeline_register(pipeline, resample_filter, "resample");

    ESP_LOGI(TAG, "[2.7] Create raw stream");
    raw_stream_cfg_t raw_cfg = {
        .out_rb_size = 8 * 1024,
        .type = AUDIO_STREAM_READER,
    };
    audio_element_handle_t raw_reader = raw_stream_init(&raw_cfg);
    audio_pipeline_register(pipeline, raw_reader, "raw");
    // end audio commands

    // start talking clock
    ESP_LOGI(TAG, "[2.2] Create fatfs stream to read data from sdcard");
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    audio_element_handle_t fatfs_stream_reader = fatfs_stream_init(&fatfs_cfg);
    audio_pipeline_register(pipeline, fatfs_stream_reader, "fatfs");

    ESP_LOGI(TAG, "[2.6] Create mp3 decoder");
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    audio_element_handle_t music_decoder = mp3_decoder_init(&mp3_cfg);
    audio_pipeline_register(pipeline, music_decoder, "music");
    // end talking clock

    //start bluetooth
    ESP_LOGI(TAG, "[3.2] Create bluetooth service");
    audio_element_handle_t bt_stream_reader = bluetooth_service_create_stream();
    if(bt_stream_reader == NULL){
        ESP_LOGE(TAG, "Bluetooth service creation returned null");
        return ESP_FAIL;
    }
    audio_pipeline_register(pipeline, bt_stream_reader, "bt");
    //end bluetooth

    isInitialized = true;
    return ESP_OK;
};

esp_err_t pipeline_reset()
{

    if (!isRunning)
    {
        ESP_LOGW(TAG, "pipline not yet active.");
        return ESP_FAIL;
    }
    esp_err_t err = ESP_OK;
    err = audio_pipeline_stop(pipeline);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Refused to stop, %s", esp_err_to_name(err));
        return err;
    }
    err = audio_pipeline_wait_for_stop(pipeline);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Timed out trying to stop, %s", esp_err_to_name(err));
        // return err;
    }
    err = audio_pipeline_reset_ringbuffer(pipeline);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Refused to reset ringbuffer, %s", esp_err_to_name(err));
        return err;
    }
    err = audio_pipeline_reset_items_state(pipeline);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Refused to reset state of items, %s", esp_err_to_name(err));
        return err;
    }

    ESP_LOGE(TAG, "reset succesfull, %s", esp_err_to_name(err));
    isRunning = false;
    return err;
};

audio_pipeline_handle_t *pipeline_set(const char *config[3], int num_elements)
{
    if (!isInitialized)
    {
        ESP_LOGI(TAG, "Pipeline not initilized. initializing now");
        pipeline_init();
    }

    if (isRunning)
    {
        pipeline_reset();
    }

    ESP_LOGW(TAG, "[ * ] Rewiring pipeline");
    esp_err_t err = audio_pipeline_link(pipeline, &config[0], num_elements);

    if (err != ESP_OK)
    {
        ESP_LOGW(TAG, "pipeline reling failed");
    }
    hasPipeline = true;

    return &pipeline;
};

esp_err_t pipeline_delete()
{

    ESP_LOGI(TAG, "[ 6 ] Stop audio_pipeline");
    audio_pipeline_stop(pipeline);
    audio_pipeline_wait_for_stop(pipeline);
    audio_pipeline_terminate(pipeline);
    audio_pipeline_remove_listener(pipeline);
    audio_pipeline_deinit(pipeline);

    return ESP_OK;
}
