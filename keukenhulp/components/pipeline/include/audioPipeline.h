#ifndef AUDIO_PIPE_H
#define AUDIO_PIPE_H

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_err.h"
#include "esp_log.h"
#include "board.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "http_stream.h"
#include "fatfs_stream.h"
#include "mp3_decoder.h"
#include "i2s_stream.h"
#include "aac_decoder.h"
#include "raw_stream.h"
#include "filter_resample.h"
#include "bluetooth_service.h"

#define GOERTZEL_SAMPLE_RATE_HZ 8000                                  // Sample rate in [Hz]
#define GOERTZEL_BUFFER_LENGTH (100 * GOERTZEL_SAMPLE_RATE_HZ / 1000) // Buffer length in samples
#define AUDIO_SAMPLE_RATE 48000                                       // Audio capture sample rate [Hz]

extern const char *radio_config[3];
extern const char *audio_config[3];
extern const char *commands_config[3];
const char *bt_config[2];

audio_pipeline_handle_t *pipeline_set(const char *config[3], int);
esp_err_t pipeline_delete();
esp_err_t pipeline_init();
esp_err_t pipeline_start();
esp_err_t pipeline_reset();

#endif